import { useState, useEffect } from "react";

const useFetchAsync = Url => {
  const [list, setList] = useState([]);

  async function getData() {
    const response = await fetch(Url);
    const data = await response.json();
    setList(data);
  }

  useEffect(() => {
    getData();
  }, []);
  return list;
};

export default useFetchAsync;
