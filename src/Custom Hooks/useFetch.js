import { useState, useEffect } from "react";

const UseFetch = Url => {
  const [list, setList] = useState([]);

  useEffect(() => {
    fetch(Url)
      .then(response => response.json())
      .then(data => setList(data));
  }, []);
  return list;
};

export default UseFetch;
