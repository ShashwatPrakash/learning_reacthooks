import { useState, useEffect } from "react";

const Example4 = props => {
  const [list, setList] = useState([]);

  useEffect(() => {
    fetch("http://localhost:1000/api/users")
      .then(response => response.json())
      .then(data => setList(data));
  }, []);
  return props.render(list);
};

export default Example4;
