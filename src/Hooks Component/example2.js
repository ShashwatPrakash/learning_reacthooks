import React, { useState } from "react";

const Example2 = props => {
  const [buttonText, setButtonText] = useState(props.text);

  return (
    <React.Fragment>
      <button onClick={() => setButtonText("Succesfully Clicked!!!")}>
        {buttonText}
      </button>
    </React.Fragment>
  );
};

export default Example2;
