import React, { useState, useEffect } from "react";

const Example3 = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    fetch("http://localhost:1000/api/users")
      .then(response => response.json())
      .then(data => setList(data));
  }, []);

  return (
    <React.Fragment>
      <ul>
        {list.map(details => (
          <li key={details._id}>{details.fullname}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

export default Example3;
