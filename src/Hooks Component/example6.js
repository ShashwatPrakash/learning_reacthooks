import React from "react";
import useFetchAsync from "../Custom Hooks/useFetchAsync";

const Example6 = () => {
  const data = useFetchAsync("http://localhost:1000/api/users");

  return (
    <React.Fragment>
      <ul>
        {data.map(details => (
          <li key={details._id}>{details.createdDate}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

export default Example6;
