import React, { useState } from "react";

const Example1 = () => {
  const [buttonText, updateButtonText] = useState("Click");

  const handleClick = () => {
    return updateButtonText("Clicked!!!");
  };

  return <button onClick={handleClick}>{buttonText}</button>;
};

export default Example1;
