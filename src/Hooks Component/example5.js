import React from "react";
import UseFetch from "../Custom Hooks/useFetch";

const Example5 = () => {
  const data = UseFetch("http://localhost:1000/api/users");

  return (
    <React.Fragment>
      <ul>
        {data.map(details => (
          <li key={details._id}>{details.password}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

export default Example5;
