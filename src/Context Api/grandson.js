import React, { Component } from "react";
import { MyContext } from "./father";
class GrandSon extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <MyContext.Consumer>
          {data => (
            <li>
              Your grandfather name is{" "}
              <span>
                <strong>{data}</strong>
              </span>
              .
            </li>
          )}
        </MyContext.Consumer>
      </React.Fragment>
    );
  }
}

export default GrandSon;
