import React, { Component, Fragment } from "react";
import Son from "./son";

export const MyContext = React.createContext();
class Father extends Component {
  state = {
    name: "Shashwat"
  };
  render() {
    return (
      <MyContext.Provider value={this.state.name}>
        <Fragment>
          <Son />
        </Fragment>
      </MyContext.Provider>
    );
  }
}

export default Father;
