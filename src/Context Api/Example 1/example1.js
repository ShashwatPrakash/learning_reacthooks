import React, { Component } from "react";
import ConsumerComponent from "./consumerComponent";

class Example1 extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <h3>This is an Example1.js</h3>
        <ConsumerComponent />
      </React.Fragment>
    );
  }
}

export default Example1;
