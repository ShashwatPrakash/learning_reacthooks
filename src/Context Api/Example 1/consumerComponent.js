import React, { Component } from "react";
import { UserConsumer } from "./providerComponent";

class ConsumerComponent extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <UserConsumer>
          {({ data, Increment, Decrement }) => (
            <div>
              <p>
                Name: <strong>{data.name}</strong>
              </p>
              <p>
                Age: <strong>{data.age}</strong>
              </p>
              <button onClick={Increment}>+</button>
              {"         "}
              <button onClick={Decrement}>-</button>
              <p>
                Contact: <strong>{data.contact}</strong>
              </p>
              <p>
                Place: <strong>{data.place}</strong>
              </p>
            </div>
          )}
        </UserConsumer>
      </React.Fragment>
    );
  }
}

export default ConsumerComponent;
