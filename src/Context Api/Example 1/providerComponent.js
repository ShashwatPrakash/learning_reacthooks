import React, { Component } from "react";
import Example1 from "./example1";

export const UserContext = React.createContext();
export const UserProvider = UserContext.Provider;
export const UserConsumer = UserContext.Consumer;

class ProvideComponent extends Component {
  state = {
    name: "Shashwat",
    age: 24,
    contact: "9066740766",
    place: "Chapra, Bihar"
  };

  handleClickInc = () => {
    this.setState({
      age: this.state.age + 1
    });
  };

  handleClickDec = () => {
    this.setState({
      age: this.state.age - 1
    });
  };
  render() {
    const ContextData = {
      data: this.state,
      Increment: this.handleClickInc,
      Decrement: this.handleClickDec
    };
    return (
      <React.Fragment>
        <UserProvider value={ContextData}>
          <h1>This is ProviderComponent.js</h1>
          <Example1 />
        </UserProvider>
      </React.Fragment>
    );
  }
}

export default ProvideComponent;
