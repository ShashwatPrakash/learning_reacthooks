import React, { Component } from "react";
import GrandSon from "./grandson";

class Son extends Component {
  render() {
    return (
      <React.Fragment>
        <h2>Son Component</h2>
        <h3>GrandSon Component</h3>
        <GrandSon />
      </React.Fragment>
    );
  }
}

export default Son;
