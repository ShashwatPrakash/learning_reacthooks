import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Example1 from "./Hooks Component/example1";
import Example2 from "./Hooks Component/example2";
import Example3 from "./Hooks Component/example3";
import Example4 from "./Hooks Component/example4";
import Example5 from "./Hooks Component/example5";
import Example6 from "./Hooks Component/example6";
import Father from "./Context Api/father";
import ProvideComponent from "./Context Api/Example 1/providerComponent";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React Hooks</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Example1 />
        <hr />
        <Example2 text="Click It!!!" />
        <hr />
        <Example3 />
        <hr />
        <Example4
          render={data => {
            return (
              <React.Fragment>
                <ul>
                  {data.map(details => (
                    <li key={details._id}>{details.username}</li>
                  ))}
                </ul>
              </React.Fragment>
            );
          }}
        />
        <hr />
        <Example5 />
        <hr />
        <Example6 />
        <hr />
        <h1>Context API Example</h1>
        <Father />
        <hr />
        <ProvideComponent />
      </div>
    );
  }
}

export default App;
